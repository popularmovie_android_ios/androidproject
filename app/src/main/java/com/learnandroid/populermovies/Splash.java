package com.learnandroid.populermovies;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by iosdeveloper on 11/15/16.
 */

public class Splash extends AppCompatActivity{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        final Thread thread = new Thread(){
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                    // Do some stuff
                } catch (Exception e) {
                    e.getLocalizedMessage();
                } finally {
                    Intent intent = new Intent(Splash.this,Principal.class);
                    startActivity(intent);
                    finish();
                }
            }

        };
        thread.start();
    }
}
